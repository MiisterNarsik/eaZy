# eaZy Way
For those who don't like to use frameworks like: symfony, laravel
Use this skeleton application to quickly setup and start working on a new PHP MVC application

## Install the Application

Run this command from the directory in which you want to install your new eaZy PHP application.

```bash
composer create-project eazy-way/easy [my-app-name]
```

Replace `[my-app-name]` with the desired directory name for your new application.