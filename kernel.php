<?php

use App\src\Service\DotEnv;
use App\src\Service\Router\Router;

require_once (__DIR__ . '/vendor/autoload.php');

//load Utils
(new Utils())->load();

//load env vars
(new DotEnv(__DIR__ . '/.env.local'))->load();

//TODO:: add global configuration here
//start router
$route = new Router();

try {
    $route->run();
} catch (Exception $e) {
    die('404');
}
