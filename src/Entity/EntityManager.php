<?php


namespace App\src\Entity;

use App\src\Service\DB\AbstractSimpleQueryBuilder;

/**
 * Class EntityManager
 * @package App\src\Service\Entity
 */
abstract class EntityManager extends AbstractSimpleQueryBuilder
{

}