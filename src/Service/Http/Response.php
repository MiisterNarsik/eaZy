<?php


namespace App\src\Service\Http;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Class Response
 * @package App\src\Service\http
 */
class Response
{
    /**
     * @param string $view
     * @param array $data
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $view, array $data = [])
    {
        $loader = new FilesystemLoader( 'templates/');
        $twig = new Environment($loader);

        echo $twig->render($view, $data);
        exit();
    }

    /**
     * Redirection
     * @param string $root
     */
    public function redirectTo(string $root)
    {
        //redirection
        header("Location: " . HTTP . $root);
        exit();
    }

    /**
     * @param array $data
     * @return string
     */
    public function json(array $data) :string
    {
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
        exit;
    }

    /**
     * @param string $file
     * @return false|int
     */
    public function fileResponse(string $file)
    {
        $fileName = basename($file);
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$fileName");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");

        return readfile($file);
    }

    /**
     * @param int $code
     * @param string $message
     */
    public function httpResponse(int $code, string $message) : void
    {
        $data = ['message' => $message];

        http_response_code($code);
        echo json_encode($data);
        exit();
    }
}