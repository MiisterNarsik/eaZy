<?php


namespace App\src\Service\Http;

/**
 * Class Request
 * @package App\src\Service\http
 */
class Request
{
    public ?string $request;

    private ?string $action;

    private ?array $params;

    private ?array $query;

    private string $method;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
        if (isset($_GET['p'])) {
            $data = explode('/', $_GET['p']);
            $this->buildRequest($data);
        }
    }

    /**
     * @param array $data
     */
    private function buildRequest(array $data)
    {
        //set controller
        $this->request = $data[0] ?? null;
        //set action
        $this->action = $data[1] ?? "index";

        if (count($data) > 2) {
            unset($data[0], $data[1]);
            $this->params = $data;
        } else {
            $this->params = [];
        }

        if (isset($_POST)) {
            $this->query = $_POST;
        }

        $this->method = $_SERVER['REQUEST_METHOD'];

    }

    /**
     * @return string|null
     */
    public function getRequest(): ?string
    {
        return $this->request;
    }

    /**
     * @param string|null $request
     */
    public function setRequest(?string $request): void
    {
        $this->request = $request;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     */
    public function setAction(?string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array|null $params
     */
    public function setParams(?array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return array|null
     */
    public function getQuery(): ?array
    {
        return $this->query;
    }

    /**
     * @param array|null $query
     */
    public function setQuery(?array $query): void
    {
        $this->query = $query;
    }
}