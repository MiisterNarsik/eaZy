<?php


namespace App\src\Service\DB;

use Exception;
use PDO;

/**
 * Class AbstractSimpleQueryBuilder
 * @package App\src\Service\DB
 */
class AbstractSimpleQueryBuilder
{
    private ?PDO $bd;

    /**
     * AbstractAdmin constructor.
     */
    public function __construct()
    {
      //set connection
        $this->bd = $this->setConnection();
    }

    /**
     * @param int $id
     * @return false|mixed|null
     * @throws Exception
     */
    protected function find(int $id)
    {
        $result = $this->findBy(['id' => $id], 1);

        if(!empty($result)) {
            return array_shift($result);
        }

        return false;
    }

    /**
     * Find row by field name and value, $params = ["field" => value], get One result by param, table
     *
     * @param array $params
     * @param array $order
     * @return mixed
     * @throws Exception
     */
    protected function findOneBy(array $params, array $order = []): bool
    {
        $result = $this->findBy($params, 1);

        if(!empty($result)) {
            return array_shift($result);
        }

        return false;
    }

    /**
     * Find all elements of a $table
     *
     * @param string $table
     * @param int $limit
     * @param array $order
     * @return array
     */
    protected function findAll(string $table, int $limit , array $order = []): array
    {
        $rq = "SELECT * FROM " .$table;

        if(!empty($order)){
            $i = 0;
            foreach ($order as $by => $val){
                if($i == 0) {
                    $rq .= " ORDER BY ".$by." ".$val;
                }else{
                    $rq .= ", ".$by." " .$val;
                }
                $i++;
            }
        }

        $rq .= " LIMIT ".$limit;

        $sql = $this->bd->prepare($rq);

        $sql->execute();

        return $sql->fetchAll();
    }

    /**
     * Find multiples row by one param or more
     *
     * @param array $params
     * @param int|null $limit
     * @param array $order
     * @return array
     * @throws Exception
     */
    protected function findBy(array $params,int $limit, array $order = []): array
    {
        $fields = array_keys($params);
        $value = array_values($params);


        $checkFields = $this->checkField($fields);
        if(!empty($checkFields)){
            throw new Exception("Fields ".implode(", ", $checkFields)." not found in table ".$this->getTableName());
        }


        $sql = "SELECT * FROM " . $this->getTableName() . " WHERE ";

        $constructSQL = $this->constructSQL($fields, $value, $order);
        $sql .= $constructSQL['sql'];
        $sql .= " LIMIT ".$limit;
        $req = $this->bd->prepare($sql);
        $req->execute($constructSQL['values']);

        return $req->fetchAll();
    }

    /**
     * Update row by field name and value, $params = ["field" => $value , ...,"id" => $id], id should be the last key in params []
     *
     * @param array $params
     */
    protected function updateById(array $params)
    {
        $sql = "UPDATE ".$this->getTableName()." SET";
        $fields = array_keys($params);
        array_pop($fields);

        foreach ($fields as $field) {
            $sql .= " $field = ? ,";
        }
        $sql = rtrim($sql, ',');
        $sql .= "WHERE id = ? LIMIT 1";
        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute(array_values($params));
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * Insert data in db , $params = ["field" => $value]
     *
     * @param array $params
     * @return string
     */
    protected function set(array $params): string
    {
        $fields = implode(",",array_keys($params));
        $firstValues = array_keys($params);

        $values = [];
        foreach ($firstValues as $value){
            $newValue = ':'.$value;
            $values[] = $newValue;
        }
        $values = implode(",",$values);

        $sql = "REPLACE INTO ".$this->getTableNAme()." (".$fields.") VALUES (".$values.")";

        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute($params);
            return $this->bd->lastInsertId();
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * Delete one element by id
     *
     * @param array $params
     * @return string
     */
    protected function deleteById(array $params): string
    {
        $sql = "DELETE FROM ". $this->getTableName() ." WHERE id = ?";

        try {
            $rq = $this->bd->prepare($sql);
            $rq->execute(array($params['id']));
            return $this->bd->lastInsertId();
        } catch (Exception $e) {
            die('#17 Erreur lors du transfert des données : ' . $e);
        }
    }

    /**
     * To check if a field exist in  $table
     *
     * @param array $fields
     * @return array
     */
    private function checkField(array $fields): array
    {
        $rows = $this->bd->query("SHOW COLUMNS FROM ".$this->getTableName());
        $results = $rows->fetchAll();

        foreach ($fields as $key => $fieldSearch) {
            foreach ($results as $result) {
                if($fieldSearch == $result->Field){
                    unset($fields[$key]);
                }
            }
        }

        return $fields;
    }

    /**
     * @param array $fields
     * @param array $values
     * @param array $order
     * @return array
     */
    private function constructSQL(array $fields, array $values, array $order = []): array
    {
        $sql = "";
        $i = 0;
        foreach ($fields as $field) {

            //Adding multiple colonne (created_at, created_at_bis, created_at_ter)
            $multiplie = ['_bis', '_ter', '_ter1', '_ter2'];
            if (str_replace($multiplie, '', $field[$i]) != $field[$i]) {
                $field[$i] = str_replace($multiplie, '', $field[$i]);
            }

            //Gestion premier params
            if ($i == 0) {
                $and = " ";
            } else {
                $and = " AND ";
            }

            //Gestion opérateur findBy(['champs' => 'operateur value']
            $operators = ['<', '<=', '>', '>=', '!=', 'LIKE'];

            //If true c'est qu'il y a un opérateur au dessus ;)
            if (str_replace($operators, '', $values[$i]) != $values[$i]) {
                $explodeOperators = explode(' ', $values[$i]);
                $sql .= " $and " . $fields[$i] . ' ' . $explodeOperators[0] . " ? ";
                $values[$i] = str_replace($operators, '', $values[$i]);
                unset($explodeOperators[0]);
                $values[$i] = implode(' ', $explodeOperators);
            } else {
                //Pas d'opérateur mais possibilité d'avoir is ~not null
                if ($values[$i] == "IS NOT NULL") {
                    $sql .= " $and " . $fields[$i] . " IS NOT NULL ";
                    unset($values[$i]);
                } elseif ($values[$i] == "IS NULL") {
                    $sql .= " $and " . $fields[$i] . " IS NULL ";
                    unset($values[$i]);
                } else {
                    //Sinon requête normal
                    $sql .= " $and " . $fields[$i] . " = ? ";
                }
            }



            $i++;
        }

        if(!empty($order)){
            $i = 0;
            foreach ($order as $by => $val){
                if($i == 0) {
                    $sql .= " ORDER BY ".$by." ". $val;
                }else{
                    $sql .= ", ".$by." ". $val;
                }
                $i++;
            }
        }

        return array('sql' => $sql, 'values' => $values);
    }

    /**
     * @return PDO
     */
    public function setConnection()
    {
        return Connection::getInstance()->getConnection();
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        $className = explode("\\",get_class($this));
        $camel = $this->camelToSnake($className[3]);
        return str_replace('_entity', '', $camel);
    }

    /**
     * @param string $input
     * @param bool $upper
     * @return string
     */
    public function camelToSnake(string $input, bool $upper = false): string
    {
        $snake = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
        if ($upper) {
            return ucfirst($snake);
        }

        return $snake;
    }

    /**
     * add entity data in db
     * @param bool $force
     * @return string
     */
    public function persist(bool $force = false): string
    {
        $data = [];
        foreach ($this as $key => $value) {
            $data[$key] = $value;
        }

        if ($force) {
            $rq = $this->bd->prepare('SET FOREIGN_KEY_CHECKS=0');
            $rq->execute();
            $data = $this->set($data);
            $rq = $this->bd->prepare('SET FOREIGN_KEY_CHECKS=1');
            $rq->execute();
            return $data;
        }

        return $this->set($data);
    }

    /**
     * remove entity data in db
     * @return string
     */
    public function delete(): string
    {
        return $this->deleteById(['id' => $this->getId()]);
    }

    /**
     * @param $object
     */
    public function cast($object)
    {
        if (is_array($object) || is_object($object)) {
            foreach ($object as $key => $value) {
                if ($value === "") {
                    $value = null;
                }

                if (property_exists($this, $key)) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * @param $object
     * @return array
     */
    public function castArray($object): array
    {
        $items = [];
        foreach ($object as $item) {
            $obj = new $this($item);
            $items[] = $obj;
        }

        return $items;
    }



}