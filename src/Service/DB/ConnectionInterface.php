<?php


namespace App\src\Service\DB;

/**
 * Interface ConnectionInterface
 * @package App\src\Service\DB
 */
interface ConnectionInterface
{
    public static function getInstance();

    public function getConnection();

}