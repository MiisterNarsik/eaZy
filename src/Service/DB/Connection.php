<?php


namespace App\src\Service\DB;

use PDO;

/**
 * Class Connection
 * @package App\src\Service\DB
 */
class Connection implements ConnectionInterface
{
    private static ?Connection $instance = null;
    private PDO $conn;

    private ?string $host;
    private ?string $name;
    private ?string $user;
    private ?string $password;


    /**
     * DbConnection constructor.
     */
    private function __construct()
    {
        $this->setEnvData();
        try {
            $this->conn = new PDO(
                sprintf('mysql:host=%s;dbname=%s;',  $this->host, $this->name),
                $this->user,
                $this->password,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
            $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (\Exception $e) {
            die('Erreur dataBase #1 : ' . $e->getMessage());
        }
    }

    /**
     * @return Connection|null
     */
    public static function getInstance(): ?Connection
    {
        if (!self::$instance) {
            self::$instance = new Connection();
        }

        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->conn;
    }


    private function setEnvData() :void
    {
        $params = explode('@',DATABASE_URL);
        $user = explode(':',$params[0]);
        $host = explode('/',$params[1]);

        $this->user = $user[0];
        $this->password = $user[1];
        $this->host = $host[0];
        $this->name = $host[1];
    }

}