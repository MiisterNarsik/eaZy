<?php


namespace App\src\Service\Router;


use App\src\Service\Http\Request;

/**
 * Class Router
 * @package App\src\Service\Router
 */
class Router
{
    public const NO_ROUTE = 404;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * @var string
     */
    private string $route;


    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->request = new Request();
        $this->route = ucfirst($this->request->getRequest()) . 'Controller';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function run()
    {
        $class = "\App\src\Controller\\" . $this->route;

        if (!class_exists($class, true)) {
            throw new \RuntimeException(
                'No route found for ' . $this->route,
                self::NO_ROUTE
            );
        }

        if (!method_exists($class, $this->request->getAction())) {
            throw new \RuntimeException(
                'No route found for ' . $this->request->getAction(),
                self::NO_ROUTE
            );
        }

        $controller = new $class();
        return call_user_func_array([$controller, $this->request->getAction()], $this->request->getParams());
    }

}