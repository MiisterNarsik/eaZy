<?php


namespace App\src\Command;

/**
 * Interface CommandInterface
 *
 * @package App\src\Command
 */
interface CommandInterface
{
    public function execute();
}