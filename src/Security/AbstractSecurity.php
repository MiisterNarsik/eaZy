<?php


namespace App\src\Security;

/**
 * Class AbstractSecurity
 * @package App\src\Security
 */
class AbstractSecurity
{
    /**
     * @return mixed|null
     */
    protected function getUser()
    {
        if(!empty($_SESSION['user_security'])){
            $um = new UserManager();
            return $um->getUserToken()->getUser();
        }else{
            return [];
        }
    }
}
