<?php


namespace App\src\Security;



use App\Classes\Entity\UserInterface;

/**
 * Class PasswordEncoder
 * @package App\src\Security
 */
class PasswordEncoder
{
    private $cost = 10;

    /**
     * @param string $plainPassword
     * @return string
     */
    public function cryptPassword(string $plainPassword): string
    {
        return password_hash($plainPassword, PASSWORD_BCRYPT, ['cost' => $this->cost]);
    }

    /**
     * @param  $user
     * @param string $plainPassword
     * @return bool
     */
    public function isPasswordValid(UserInterface $user, string $plainPassword): bool
    {
        if ($user->getPassword() != null) {
            return password_verify($plainPassword, $user->getPassword());
        }

        return false;
    }

    /**
     * @param int $cost
     */
    public function setCost(int $cost): void
    {
        if ($cost < 4 || $cost > 12) {
            throw new \InvalidArgumentException('Cost must be in the range of 4-31.');
        }
        $this->cost = $cost;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateToken(int $length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generatePassword($length = 6)
    {
        $pw = '';

        for ($i = 1; $i <= $length; $i++) {
            $pw .= (string)rand(0, 9);
        }

        return $pw;
    }

}
