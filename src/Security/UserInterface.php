<?php

namespace App\src\Security;

/**
 * Class UserInterface
 * @package App\Classes\Entity
 */
interface UserInterface
{
    public function getEmail(): ?string;

    public function getPassword(): ?string;

    public function getRoles(): array;

    public function isEnabled(): bool;

    public function getAccessToken(): string;
}
