<?php


namespace App\Classes\Security;




use App\Classes\Entity\UserInterface;

/**
 * Class UserToken
 * @package App\Classes\Security
 */
class UserToken
{

    private $user;

    /**
     * UserToken constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize($this);
    }
}