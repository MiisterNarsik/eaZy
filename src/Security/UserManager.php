<?php


namespace App\src\Security;

use App\Classes\Security\ApiTokenAuthenticator;
use App\Classes\Security\UserToken;
use ReflectionException;

/**
 * Class UserManager
 * @package App\src\Security
 */
class UserManager
{
    const DEFAULT_PREFIX_KEY = 'user_security';
    const FORK_PREFIX_KEY = 'fork_user';
    const CSRF_TOKEN = 'csrf_security';

    /**
     * UserManager constructor.
     */
    public function __construct()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * @return mixed|null
     */
    public function getUserToken()
    {
        $userToken = null;
        if ($this->hasUserToken()) {
            $userToken = unserialize($_SESSION[self::DEFAULT_PREFIX_KEY]);
        }

        return $userToken;
    }

    /**
     * @return bool
     */
    public function hasUserToken(): bool
    {
        $key = self::DEFAULT_PREFIX_KEY;
        return (array_key_exists($key, $_SESSION) && unserialize($_SESSION[$key]) !== false);
    }

    /**
     * @param array $roles
     * @return bool
     * @throws ReflectionException
     */
    public function isGranted(array $roles): bool
    {
        if (is_null($userToken = $this->getUserToken())) {
            return false;
        }

        foreach ($roles as $roleRequis){
            foreach ($userToken->getUser()->getRoles() as $role){

                $arrayConstantRole = (new \ReflectionClass(new Users()))->getConstants();
                if(array_key_exists($role, $arrayConstantRole) && array_key_exists($roleRequis, $arrayConstantRole)){
                    if(constant(Users::class.'::'.$role) >= constant(Users::class.'::'.$roleRequis)){
                        return true;
                    }
                }else {
                    die('Erreur de ROLE ' . $role . ' ou ' . $roleRequis);
                }
            }
        }

        return false;
    }

    /**
     * @param UserInterface $user
     * @param null $oldUser
     * @return UserToken
     */
    public function createUserToken(UserInterface $user, $oldUser = null)
    {
        $tokenGenerator = new PasswordEncoder();
        if($oldUser != null){
            $_SESSION[self::FORK_PREFIX_KEY] = $oldUser;
        }
        $userToken = new UserToken($user);
        $_SESSION[self::DEFAULT_PREFIX_KEY] = $userToken->serialize();
        $_SESSION[self::CSRF_TOKEN] = $tokenGenerator->generateToken(60);

        return $userToken;
    }

    /**
     * @return void
     */
    public function logout()
    {
        if ($this->hasUserToken()) {
            unset($_SESSION[self::DEFAULT_PREFIX_KEY]);
            unset($_SESSION[self::CSRF_TOKEN]);
        }
    }

    /**
     * @return ApiTokenAuthenticator
     */
    public function apiTokenAuthenticator()
    {
        return new ApiTokenAuthenticator();
    }

    /**
     * @return mixed|null
     */
    public function getCsrfToken()
    {
        $csrfToken = null;
        if ($this->hasCsrfToken()) {
            $csrfToken = $_SESSION[self::CSRF_TOKEN];
        }

        return $csrfToken;
    }

    /**
     * @return bool
     */
    public function hasCsrfToken()
    {
        $key = self::CSRF_TOKEN;
        return (array_key_exists($key, $_SESSION) && $_SESSION[$key] !== null);
    }

}
