<?php


namespace App\Classes\Security;

use App\src\Security\AbstractSecurity;
use App\src\Service\Entity\EntityManager;
use Exception;

/**
 * Class ApiTokenAuthenticator
 * @package App\Classes\Security
 */
class ApiTokenAuthenticator extends AbstractSecurity
{
    const TOKEN_NAME = 'access_token';

    /**
     * @var EntityManager
     */
    private EntityManager $em;

    /**
     * ApiTokenAuthenticator constructor.
     */
    public function __construct()
    {
        $this->em = new EntityManager();
    }

    /**
     * @param string $token
     * @return bool|void
     * @throws Exception
     */
    public function isAuthorized(string $token)
    {
        $result = $this->em->findOneBy(['access_token' => $token, 'is_enabled' => 1], 'users');
        if ($result) {
            return true;
        }

        $this->onAuthenticationFailure();
    }

    /**
     * @return void
     */
    public function onAuthenticationFailure()
    {
        $data = ['message' => 'Auth token is not valid'];
        http_response_code(403);
        echo json_encode($data);
        exit();
    }

    /**
     * @Benoit
     * @return string|null
     */
    public function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * @Benoit
     * @return mixed|null
     */
    public function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }


}
