<?php


namespace App\src\Controller;

use App\src\Entity\TestEntity;

/**
 * Class TestController
 * @package App\src\Controller
 */
class TestController extends AbstractController
{
    public function index()
    {
        $test = new TestEntity();
        dd($test);
    }
}