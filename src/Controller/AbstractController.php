<?php


namespace App\src\Controller;


use App\src\Service\Http\Response;

/**
 * Class AbstractController
 * @package App\src\Controller
 */
abstract class AbstractController
{
    protected Response $response;

    public function __construct()
    {
        $this->response = new Response();
    }
}